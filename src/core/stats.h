#ifndef LANIAKEA_CORE_STATS_H
#define LANIAKEA_CORE_STATS_H


#include "pbrt.h"
#include <map>
#include <chrono>
#include <string>
#include <functional>


class StatsAccumulator;


class StatRegisterer {
public:
    StatRegisterer(std::function<void(StatsAccumulator &)> func) {
        if (!funcs)
            funcs = new std::vector<std::function<void(StatsAccumulator &)>>;
        funcs->push_back(func);
    }

private:
    static std::vector<std::function<void(StatsAccumulator &)>> *funcs;
};


void PrintStats(FILE *dest);

void ReportThreadStats();


class StatsAccumulator {
public:
    void ReportCounter(const std::string &name, int64_t val) {
        counters[name] += val;
    }

    void ReportMemoryCounter(const std::string &name, int64_t val) {
        memoryCounters[name] += val;
    }

    void ReportIntDistribution(const std::string &name, int64_t sum, int64_t count, int64_t min, int64_t max) {
        intDistributionSums[name] += sum;
        intDistributionCounts[name] += count;

        if (intDistributionMins.find(name) == intDistributionMins.end())
            intDistributionMins[name] = min;
        else
            intDistributionMins[name] = std::min(intDistributionMins[name], min);

        if (intDistributionMaxs.find(name) == intDistributionMaxs.end())
            intDistributionMaxs[name] = max;
        else
            intDistributionMaxs[name] = std::max(intDistributionMaxs[name], max);
    }

    void ReportFloatDistribution(const std::string &name, double sum, int64_t count, double min, double max) {
        floatDistributionSums[name] += sum;
        floatDistributionCounts[name] += count;

        if (floatDistributionMins.find(name) == floatDistributionMins.end())
            floatDistributionMins[name] = min;
        else
            floatDistributionMins[name] = std::min(floatDistributionMins[name], min);

        if (floatDistributionMaxs.find(name) == floatDistributionMaxs.end())
            floatDistributionMaxs[name] = max;
        else
            floatDistributionMaxs[name] = std::max(floatDistributionMaxs[name], max);
    }

    void ReportPercentage(const std::string &name, int64_t num, int64_t denom) {
        percentage[name].first += num;
        percentage[name].second += denom;
    }

    void ReportRatio(const std::string &name, int64_t num, int64_t denom) {
        ratios[name].first += num;
        ratios[name].second += denom;
    }

    void ReportTimer(const std::string &name, int64_t val) {
        timers[name] += val;
    }

    void Print(FILE *file);

private:
    std::map<std::string, int64_t> counters;
    std::map<std::string, int64_t> memoryCounters;
    std::map<std::string, int64_t> intDistributionSums;
    std::map<std::string, int64_t> intDistributionCounts;
    std::map<std::string, int64_t> intDistributionMins;
    std::map<std::string, int64_t> intDistributionMaxs;
    std::map<std::string, double> floatDistributionSums;
    std::map<std::string, int64_t> floatDistributionCounts;
    std::map<std::string, double> floatDistributionMins;
    std::map<std::string, double> floatDistributionMaxs;
    std::map<std::string, std::pair<int64_t, int64_t>> percentage;
    std::map<std::string, std::pair<int64_t, int64_t>> ratios;
    std::map<std::string, int64_t> timers;
};


enum class Prof {
    IntegratorRender,
    SamplerIntegratorLi,
    DirectLighting,
    AccelIntersect,
    AccelIntersectP,
    TriIntersect,
    TriIntersectP,
    ComputeScatteringFuncs,
    GenerateCameraRay,
    BSDFEvaluation,
    MergeFilmTile,
    SplatFilm,
    StartPixel,
    TexFiltTrilerp,
    TexFiltEWA,
    NumProfEvents
};


static const char *ProfNames[] = {
        "Integrator::Render()",
        "SamplerIntegrator::Li()",
        "Direct Lighting",
        "Accelerator::Intersect()",
        "Accelerator::IntersectP()",
        "Triangle::Intersect()",
        "Triangle::IntersectP()",
        "Material::ComputeScatteringFunctions()",
        "Camera::GenerateRay[Differential]()",
        "BSDF::f()",
        "BSSRDF::f()",
        "Film::MergeTile()",
        "Film::AddSplat()",
        "Sampler::StartPixelSampler()",
        "MPIMap::Lookup() (trilinear)",
        "MPIMap::Lookup() (EWA)",
};


static_assert((int)Prof::NumProfEvents == sizeof(ProfNames) / sizeof(ProfNames[0]), "ProfNames[] arra and Prof enumerant have different numbers of entries!");


extern LANIAKEA_THREAD_LOCAL uint32_t ProfilerState;

inline uint32_t CurrentProfilerState() { return ProfilerState; }


class ProfilePhase {
public:
    ProfilePhase(Prof p) {
        categoryBit = (1 << static_cast<int>(p));
        reset = (ProfilerState & categoryBit) == 0;
        ProfilerState |= categoryBit;
    }

    ~ProfilePhase() {
        if (reset) ProfilerState &= ~categoryBit;
    }

    ProfilePhase(const ProfilePhase &) = delete;

private:
    bool reset;
    uint32_t categoryBit;
};


void InitProfiler();
void SuspendProfiler();
void ResumeProfiler();
void ProfilerWorkerThreadInit();
void ReportProfilerResults(FILE *dest);
void CleanupProfiler();


#define STAT_COUNTER(title, var)\
    static LANIAKEA_THREAD_LOCAL int64_t var;\
    static void STATS_FUNC##var(StatsAccumulator &accum){\
        accum.ReportCounter(title, var);\
        var = 0;\
    }\
    static StatRegisterer STATS_REG##var(STATS_FUNC##var)

#define STAT_MEMORY_COUNTER(title, var)\
    static LANIAKEA_THREAD_LOCAL int64_t var;\
    static void STATS_FUNC##var(StatsAccumulator &accum) {\
        accum.ReportMemoryCounter(title, var);\
        var = 0;\
    }\
    static StatRegisterer STATS_REG##var(STATS_FUNC##var)



#define STATS_INT64_T_MIN std::numeric_limits<int64_t>::max()
#define STATS_INT64_T_MAX std::numeric_limits<int64_t>::lowest()
#define STATS_DBL_T_MIN std::numeric_limits<double>::max()
#define STATS_DBL_T_MAX std::numeric_limits<double>::lowest()


#define STAT_INT_DISTRIBUTION(title, var)\
    static LANIAKEA_THREAD_LOCAL int64_t var##sum;\
    static LANIAKEA_THREAD_LOCAL int64_t var##count;\
    static LANIAKEA_THREAD_LOCAL int64_t var##min = (STATS_INT64_T_MIN);\
    static LANIAKEA_THREAD_LOCAL int64_t var##max = (STATS_INT64_T_MAX);\
    static void STATS_FUNC##var(StatsAccumulator &accum){\
        accum.ReportIntDistribution(title, var##sum, var##count, var##min, var##max);\
        var##sum = 0;\
        var##count = 0;\
        var##min = std::numeric_limits<int64_t>::max();\
        var##max = std::numeric_limits<int64_t>::lowest();\
    }\
    static StatRegisterer STATS_REG##var(STATS_FUNC##var)


#define STAT_FLOAT_DISTRIBUTION(title, var)\
    static LANIAKEA_THREAD_LOCAL double var##sum;\
    static LANIAKEA_THREAD_LOCAL double var##count;\
    static LANIAKEA_THREAD_LOCAL double var##min = (STATS_DBL_T_MIN);\
    static LANIAKEA_THREAD_LOCAL double var##max = (STATS_DBL_T_MAX);\
    static void STATS_FUNC##var(StatsAccumulator &accum){\
        accum.ReportFloatDistribution(title, var##sum, var##count, var##min, var##max);\
        var##sum = 0;\
        var##count = 0;\
        var##min = std::numeric_limits<double>::max();\
        var##max = std::numeric_limits<double>::lowest();\
    }\
    static StatRegisterer STATS_REG##var(STATS_FUNC##var)


#define ReportValue(var, value)\
    do {\
        var##sum += value;\
        var#count += 1;\
        var##min = std::min(var##min, static_cast<decltype(var##min)>(value));\
        var##max = std::max(var##max, static_cast<decltype(var##max)>(value));\
    } while(0)


#define STAT_PERCENT


#define STAT_RATIO


class StatTimer {
public:
    StatTimer(uint64_t *sns) {
        sumNS = sns;
        startTime = std::chrono::high_resolution_clock::now();
    }

    ~StatTimer() {
        time_point endTime = std::chrono::high_resolution_clock::now();
        *sumNS += std::chrono::duration_cast<std::chrono::nanoseconds>(endTime - startTime).count();
    }

    typedef std::chrono::high_resolution_clock::time_point time_point;
    time_point startTime;
    uint64_t *sumNS;
};


#define STAT_TIMER(title, var)\
    static LANIAKEA_THREAD_LOCAL uint64_t var;\
    static void STATS_FUNC##var(StatsAccumulator &accum) {\
        accum.ReportTimer(title, var);\
        var = 0;\
    }\
    static StatRegisterer STATS_REG##var(STATS_FUNC##var)


#endif // LANIAKEA_CORE_STATS_H