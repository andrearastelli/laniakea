#ifndef LANIAKEA_CORE_API_H
#define LANIAKEA_CORE_API_H


#include "pbrt.h"

namespace laniakea {

    void Init(const Options &opt);
    //void Cleanup();
    //void Identity();
    //void Translate();
    //void Rotate();
    //void Scale();
    //void LookAt();
    //void ConcatTransform();
    //void Transform();

}


#endif // LANIAKEA_CORE_API_H