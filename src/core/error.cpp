#include "error.h"
//#include "stringprint.h"
//#include "progressreporter.h"
#include <mutex>

#include <stdarg.h>

#define LANIAKEA_ERROR_IGNORE 0
#define LANIAKEA_ERROR_CONTINUE 1
#define LANIAKEA_ERROR_ABORT 2

const char *findWordEnd(const char *buf) {
    while (*buf != '\0' && !isspace(*buf)) ++buf;
    return buf;
}

template <typename... Args>
static std::string StringVaprintf(const std::string &fmt, va_list args) {
    va_list argsCopy;
    va_copy(argsCopy, args);

    auto size = vsnprintf(nullptr, 0, fmt.c_str(), args) + 1;
    std::string str;
    str.resize(size);
    vsnprintf(&str[0], size, fmt.c_str(), argsCopy);
    str.pop_back();

    return str;
}

static void processError(const char *format, va_list args, const char *errorType, int disposition) {
    if (disposition == LANIAKEA_ERROR_IGNORE) return;

    std::string errorString;
    extern int line_num;
    if (line_num != 0) {
        extern std::string current_file;
        errorString += current_file;
        errorString += StringPrintf("(%d): ", line_num);
    }

    int width = std::max(20, TerminalWidth() - 2);
}