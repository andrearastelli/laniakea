#include "progressreporter.h"
//#include "parallel.h"
#include "stats.h"

#include <sys/ioctl.h>
#include <unistd.h>
#include <errno.h>


ProgressReporter::ProgressReporter(int64_t totalWork, const std::string &title):
        totalWork(std::max(static_cast<int64_t>(1), totalWork)),
        title(title),
        startTime(std::chrono::system_clock::now())
{
    workDone = 0;
    exitThread = false;

    SuspendProfiler();
    auto barrier = std::make_shared<Barrier>(2);
    updateThread = std::thread([this, barrier]() {
        ProfilerWorkerThreadInit();
        ProfilerState = 0;
        barrier->Wait();
        PrintBar();
    });

    barrier->Wait();
    ResumeProfiler();
}

ProgressReporter::~ProgressReporter() {
    workDone = totalWork;
    exitThread = true;
    updateThread.join();
    printf("\n");
}

void ProgressReporter::PrintBar() {
    auto barLength = TerminalWidth() - 28;
    auto totalPlusses = std::max(2, barLength - static_cast<int>(title.size()));
    auto plussesPrinted = 0;

    const auto bufLen = title.size() + totalPlusses + 64;
}