#ifndef LANIAKEA_CORE_PROGRESSREPORTER_H
#define LANIAKEA_CORE_PROGRESSREPORTER_H


#include "pbrt.h"
#include <atomic>
#include <chrono>
#include <thread>

class ProgressReporter {
public:
    ProgressReporter(int64_t totalWork, const std::string &title);
    ~ProgressReporter();

    void Update(int64_t num=1) {
        if (num == 0) return;
        workDone += num;
    }

    Float ElapsedMS() const {
        auto now = std::chrono::system_clock::now();
        auto elapsedMS = std::chrono::duration_cast<std::chrono::milliseconds>(now - startTime).count();
    }

    void Done();

private:
    void PrintBar();

    const int64_t totalWork;
    const std::string title;
    const std::chrono::system_clock::time_point startTime;
    std::atomic<int64_t> workDone;
    std::atomic<bool> exitThread;
    std::thread updateThread;
};

extern int TerminalWidth();


#endif // LANIAKEA_CORE_PROGRESSREPORTER_H