#ifndef LANIAKEA_CORE_PORT_H
#define LANIAKEA_CORE_PORT_H


#if defined(__LINUX__)
#define LANIAKEA_IS_LINUX
#endif

#ifdef LANIAKEA_IS_LINUX
#define LANIAKEA_HAVE_ALLOCA_H
#endif

#define LANIAKEA_THREAD_LOCAL __thread
#define LANIAKEA_HAVE_ALIGNAS

#ifndef LANIAKEA_L1_CACHE_LINE_SIZE
#define LANIAKEA_L1_CACHE_LINE_SIZE 64
#endif

#define LANIAKEA_FORCEINLINE __attribute__((always_inline)) inline
#define LANIAKEA_HAVE_ITIMER


#endif // LANIAKEA_CORE_PORT_H