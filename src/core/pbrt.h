#ifndef LANIAKEA_CORE_PBRT_H
#define LANIAKEA_CORE_PBRT_H


#include "port.h"

#include <iostream>
#include <type_traits>
#include <algorithm>
#include <cinttypes>
#include <cmath>
#include <limits>
#include <memory>
#include <string>
#include <vector>
#include <assert.h>
#include <string.h>
#include <stdint.h>
#include <alloca.h>
#include "error.h"


#define ALLOCA(TYPE, COUNT) (TYPE *) alloca((COUNT) * sizeof(TYPE))


#ifdef LANIAKEA_FLOAT_AS_DOUBLE
typedef double Float;
#else
typedef float Float;
#endif

struct Options {
    int nThreads = 0;
    bool quickRender = false;
    bool verbose = false;
    std::string imageFile;
};

extern Options LaniakeaOptions;

static constexpr Float MaxFloat = std::numeric_limits<Float>::max();
static constexpr Float Infinity = std::numeric_limits<Float>::infinity();
static constexpr Float MachineEpsilon = std::numeric_limits<Float>::epsilon() * 0.5;

const Float ShadowEpsilon = 0.0001f;

static const Float Pi = 3.14159265358979323846;
static const Float InvPi = 0.31830988618379067154;
static const Float Inv2Pi = 0.15915494309189533577;
static const Float Inv4Pi = 0.07957747154594766788;
static const Float PiOver2 = 1.57079632679489661923;
static const Float PiOver4 = 0.78539816339744830961;
static const Float Sqrt2 = 1.41421356237309504880;

inline uint32_t FloatToBits(float f) {
    uint32_t ui;
    memcpy(&ui, &f, sizeof(float));

    return ui;
}

inline float BitsToFloat(uint32_t ui) {
    float f;
    memcpy(&f, &ui, sizeof(uint32_t));

    return f;
}

inline uint64_t FloatToBits(double f) {
    uint64_t ui;
    memcpy(&ui, &f, sizeof(double));

    return ui;
}

inline double BitsToFloat(uint64_t ui) {
    double f;
    memcpy(&f, &ui, sizeof(uint64_t));

    return f;
}

inline float NextFloatUp(float v) {
    if (std::isinf(v) && v > 0.) return v;
    if (v == -0.f) v = 0.f;

    auto ui = FloatToBits(v);
    if (v >= 0)
        ++ui;
    else
        --ui;
    return BitsToFloat(ui);
}

inline float NextFloatDown(float v) {
    if (std::isinf(v) && v < 0.) return v;
    if (v == 0.f) v = -0.f;

    auto ui = FloatToBits(v);
    if (v > 0)
        --ui;
    else
        ++ui;
    return BitsToFloat(ui);
}

inline double NextFloatUp(double v, double delta=1) {
    if (std::isinf(v) && v > 0.) return v;
    if (v == -0.f) v = 0.f;

    auto ui = FloatToBits(v);
    if (v > 0.)
        ui -= delta;
    else
        ui += delta;
    return BitsToFloat(ui);
}

inline double NextFloatDown(double v, double delta=1) {
    if (std::isinf(v) && v == 0.) return v;
    if (v == 0.f) v = -0.f;

    auto ui = FloatToBits(v);
    if (v > 0.)
        ui -= delta;
    else
        ui += delta;
    return BitsToFloat(ui);
}

inline Float gamma(int n) {
    return (n * MachineEpsilon) / (1 - n * MachineEpsilon);
}

inline Float GammaCorrect(Float value) {
    if (value <= 0.0031308f) return 12.92f * value;
    return 1.055f * std::pow(value, static_cast<Float>(1.f / 2.4f)) - 0.055f;
}

inline Float InverseGammaCorrect(Float value) {
    if (value <= 0.04045f) return value * 1.f / 12.92f;
    return std::pow((value * 0.055f) * 1.f / 1.055f, static_cast<Float>(2.4f));
}

template <typename T, typename U, typename V>
inline T Clamp(T val, U low, V high) {
    if (val < low) return low;
    else if (val > high) return high;
    else return val;
}

template <typename T>
inline T Mod(T a, T b) {
    T result = a - (a / b) * b;
    return static_cast<T>((result < 0) ? result + b : result);
}

template <>
inline Float Mod(Float a, Float b) {
    return std::fmod(a, b);
}

inline Float Radians(Float deg) {
    return (Pi / 180) * deg;
}

inline Float Degree(Float rad) {
    return (180 / Pi) * rad;
}

inline Float Log2(Float x) {
    const Float invLog2 = 1.442695040888963387004650940071;
    return std::log(x) * invLog2;
}

inline int Log2Int(uint32_t v) {
    return 31 - __builtin_clz(v);
}

template <typename T>
inline bool IsPowerOf2(T v) {
    return v && !(v & (v - 1));
}

inline int32_t RoundUpPow2(int32_t v) {
    v--;
    v |= v >> 1;
    v |= v >> 2;
    v |= v >> 4;
    v |= v >> 8;
    v |= v >> 16;
    return v + 1;
}

inline int64_t RoundUpPow2(int64_t v) {
    v--;
    v |= v >> 1;
    v |= v >> 2;
    v |= v >> 4;
    v |= v >> 8;
    v |= v >> 16;
    v |= v >> 32;
    return v + 1;
}

inline int CountTrailingZeros(uint32_t v) {
    return __builtin_ctz(v);
}

template <typename Predicate>
int FindInterval(int size, const Predicate &pred) {
    auto first = 0;
    auto len = size;

    while (len > 0) {
        auto half = len >> 1;
        auto middle = first + half;

        if (pred(middle)) {
            first = middle + 1;
            len -= half + 1;
        } else
            len = half;
    }

    return Clamp(first - 1, 0, size - 2);
}

#ifdef NDEBUG
#define Assert(expr) ((void)0)
#else
#define Assert(expr) ((expr) ? (void)0 : Severe("Assertion \"%s\" failed in %s, line %d", #expr, __FILE__, __LINE__))
#endif


#endif // LANIAKEA_CORE_PBRT_H