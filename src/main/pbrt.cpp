#include "pbrt.h"
#include "api.h"
//#include "parser.h"
#include "parallel.h"

#include <cstring>

/**
 *
 * @param msg
 */
static void usage(std::string msg) {
    if (!msg.empty())
        std::cerr << "pbrt: " << msg << "\n\n" << std::endl;

    std::cerr << R"(usage: pbrt [<options>] <filename.pbrt...>)" << std::endl;
    std::cerr << R"(--cat                   Print a formatted version of the input file(s).)" << std::endl;
    std::cerr << R"(                        Does not render an image)" << std::endl;
    std::cerr << R"(--help                  Print this help text.)" << std::endl;
    std::cerr << R"(--nthread <num>         Use specified numer of threads for rendering.)" << std::endl;
    std::cerr << R"(--outfile <filename>    Write the final image to the given filename.)" << std::endl;
    std::cerr << R"(--quick                 Automatically reduce quality settings to render quickly.)" << std::endl;
    std::cerr << R"(--verbose               Print out more detailed logging informations.)" << std::endl;

    exit(1);
}

/**
 *
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char *argv[]) {

    auto filenames = std::vector<std::string>();

    for (int i=1; i<argc; ++i) {

        // nthreads
        if (!std::strcmp(argv[i], "--nthreads") || !std::strcmp(argv[i], "-nthreads")) {
            if (i + 1 == argc)
                usage("missing value after --nthreads argument");
            std::cout << "N Threads: " << atoi(argv[++i]) << std::endl;
        }
        else if (!std::strncmp(argv[i], "--nthreads=", 11)) {
            std::cout << "N Threads: " << atoi(&argv[i][11]) << std::endl;
        }
        // outfile
        else if (!std::strcmp(argv[i], "--outfile") || !std::strcmp(argv[i], "-outfile")) {
            if (i + 1 == argc)
                usage("missing value after --outfile argument");
            std::cout << "Image file: " << argv[++i] << std::endl;
        }
        else if (!std::strncmp(argv[i], "--outfile=", 10)) {
            std::cout << "Image file: " << &argv[i][10] << std::endl;
        }
        // quick
        else if (!std::strcmp(argv[i], "--quick") || !std::strcmp(argv[i], "-quick")) {
            std::cout << "Quick render." << std::endl;
        }
        // verbose
        else if (!std::strcmp(argv[i], "--verbose") || !std::strcmp(argv[i], "-verbose")) {
            std::cout << "Verbose output." << std::endl;
        }
        // help
        else if(!std::strcmp(argv[i], "--help") || !std::strcmp(argv[i], "--help") || !std::strcmp(argv[i], "-h")) {
            usage("");
            return 0;
        }
        else {
            auto filename = std::string(argv[i]);
            filenames.push_back(filename);
        }

    }

    std::cout << "Filename list:" << std::endl;
    for (auto &filename : filenames)
        std::cout << "\t" << filename << std::endl;

#ifndef NDEBUG
    std::cout << "*** DEBUG BUILD ***" << std::endl;
#endif //!NDEBUG

    if (filenames.size() == 0) {
        std::cout << "Parse scene from standard input." << std::endl;
    }
    else {
        std::cout << "Parse scene from input files." << std::endl;
        for (auto &filename : filenames)
            std::cout << "Parsing file: " << filename << std::endl;
    }

    return 0;
}